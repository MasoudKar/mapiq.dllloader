﻿using DllLoader.Plugin.Core;
using System;

namespace PluginTwo
{
	public class PluginTwo : IPlugin
	{
		public string Type
		{
			get { return "Type 2"; }
		}

		public bool BookingIsPossible(DateTime reqStart, DateTime reqEnd, DateTime bookingStart, DateTime bookingEnd)
		{
			Console.WriteLine("Just executed " + Type);
			return ((reqStart < bookingStart) && (reqEnd < bookingStart) || ((reqStart > bookingEnd) && (reqEnd > bookingEnd)));
		}
	}
}