﻿using DllLoader.DataAccess;
using System.Collections.Generic;

namespace DllLoader.Services
{
	public interface IBookingService
	{
		IEnumerable<Booking> LoadAllBookings();
		bool MakeResrvation(BookingDto data);
	}
}
