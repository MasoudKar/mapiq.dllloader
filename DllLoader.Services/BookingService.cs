﻿using DllLoader.DataAccess;
using DllLoader.Plugin.Core;
using System.Collections.Generic;
using System.Linq;

namespace DllLoader.Services
{
	public class BookingService : IBookingService
	{
		private readonly IRepository<Booking> _bookingRepo;
		private readonly IRepository<Customer> _customerRepo;
		private readonly IPluginManager _pluginManager;


		public BookingService(IPluginManager pluginManager, IRepository<Booking> bookingRepo, IRepository<Customer> customerRepo)
		{
			_pluginManager = pluginManager;
			_customerRepo = customerRepo;
			_bookingRepo = bookingRepo;

		}

		public IEnumerable<Booking> LoadAllBookings()
		{
			return _bookingRepo.LoadAll();
		}

		public bool MakeResrvation(BookingDto data)
		{
			var plugin = LoadPlugin(data.CustomerCode);
			if(plugin == null)
			{
				return false;
			}

			foreach (var booking in _bookingRepo.LoadAll())
			{
				if (!plugin.BookingIsPossible(data.StartDate, data.EndDate, booking.StartTime, booking.EndTime))
				{
					return false;
				}
			}
			_bookingRepo.Save(new Booking() { Title = data.Title, StartTime = data.StartDate, EndTime = data.EndDate });
			return true;
		}

		IPlugin LoadPlugin(int customerCode)
		{
			var customer = _customerRepo.LoadAll().Where(x => x.Code == customerCode).FirstOrDefault();
			if (customer != null)
			{
				return _pluginManager.LoadPlugin(customer.AssemblyAddress);
			}
			else
			{
				return null;
			}
			
		}
	}
}
