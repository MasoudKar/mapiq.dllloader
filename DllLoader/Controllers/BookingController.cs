﻿using DllLoader.DataAccess;
using DllLoader.Services;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;

namespace DllLoader.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class BookingController : ControllerBase
	{
		private readonly IBookingService _bookingService;

		public BookingController(IBookingService bookingService)
		{
			_bookingService = bookingService;
		}
		
		[HttpGet]
		public IEnumerable<Booking> Get()
		{
			return _bookingService.LoadAllBookings();
		}

		[HttpPost]
		public ActionResult Post(BookingDto booking)
		{
			if (_bookingService.MakeResrvation(booking))
			{
				return Ok();
			}
			else
			{
				return BadRequest("Couldn't make the reservation");
			}
		}

	
	}
}
