using System;
using Xunit;

namespace PluginOne.Test
{
    public class PluginOneTest
    {

        private PluginOne _target;

        public PluginOneTest()
        {
            _target = new PluginOne();
        }

        [Fact]
        public void BookingIsPossible_RequestIsBeforeBooking_ReturnTrue()
        {
            //Arrage
            var reqStart = new DateTime(2019, 01, 01, 12, 00, 00);
            var reqEnd = new DateTime(2019, 01, 01, 12, 30, 00);
            var bookingStart = new DateTime(2019, 01, 01, 13, 00, 00);
            var bookingEnd = new DateTime(2019, 01, 01, 13, 30, 00);

            //Act
            bool result = _target.BookingIsPossible(reqStart, reqEnd, bookingStart, bookingEnd);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void BookingIsPossible_RequestIsAfterBooking_ReturnTrue()
        {
            //Arrage
            var reqStart = new DateTime(2019, 01, 01, 12, 00, 00);
            var reqEnd = new DateTime(2019, 01, 01, 12, 30, 00);
            var bookingStart = new DateTime(2019, 01, 01, 11, 00, 00);
            var bookingEnd = new DateTime(2019, 01, 01, 11, 30, 00);
            _target = new PluginOne();

            //Act
            bool result = _target.BookingIsPossible(reqStart, reqEnd, bookingStart, bookingEnd);

            //Assert
            Assert.True(result);
        }

        [Fact]
        public void BookingIsPossible_RequestisDuringBooking_ReturnTrue()
        {
            //Arrage
            var reqStart = new DateTime(2019, 01, 01, 12, 00, 00);
            var reqEnd = new DateTime(2019, 01, 01, 13, 30, 00);
            var bookingStart = new DateTime(2019, 01, 01, 11, 00, 00);
            var bookingEnd = new DateTime(2019, 01, 01, 13, 00, 00);

            //Act
            bool result = _target.BookingIsPossible(reqStart, reqEnd, bookingStart, bookingEnd);

            //Assert
            Assert.False(result);
        }

        [Fact]
        public void BookingIsPossible_RequestStartsDuringBooking_ReturnTrue()
        {
            //Arrage
            var reqStart = new DateTime(2019, 01, 01, 12, 00, 00);
            var reqEnd = new DateTime(2019, 01, 01, 13, 30, 00);
            var bookingStart = new DateTime(2019, 01, 01, 11, 00, 00);
            var bookingEnd = new DateTime(2019, 01, 01, 13, 00, 00);

            //Act
            bool result = _target.BookingIsPossible(reqStart, reqEnd, bookingStart, bookingEnd);

            //Assert
            Assert.False(result);
        }
    }
}
