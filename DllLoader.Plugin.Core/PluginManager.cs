﻿using Microsoft.Extensions.Configuration;
using System;
using System.IO;
using System.Reflection;

namespace DllLoader.Plugin.Core
{
	public sealed class PluginManager : IPluginManager
	{

        private readonly IConfiguration _config;
		public PluginManager(IConfiguration configuration)
		{
            _config = configuration;
		}

		public IPlugin LoadPlugin(string pluginName)
		{
			var path = Path.GetFullPath(Path.Combine(Environment.CurrentDirectory, _config["PluginPath"]));
			DirectoryInfo dInfo = new DirectoryInfo(path);
			var files = dInfo.GetFiles(pluginName+".dll");
			if (files != null)
			{
				var _assembly = Assembly.LoadFile(files[0].FullName);
				Type test = _assembly.GetType($"{pluginName}.{pluginName}");
				return Activator.CreateInstance(test) as IPlugin;
			}
			else
			{
				return null;
			}
		}
	}
}
