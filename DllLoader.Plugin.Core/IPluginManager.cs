﻿namespace DllLoader.Plugin.Core
{
	public interface IPluginManager
	{
		IPlugin LoadPlugin(string pluginName);
	}
}
