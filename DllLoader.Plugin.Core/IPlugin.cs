﻿using System;

namespace DllLoader.Plugin.Core
{
	public interface IPlugin
	{
		string Type { get; }
		bool BookingIsPossible(DateTime reqStart, DateTime reqEnd, DateTime bookingStart, DateTime bookingEnd);
	}
}
