﻿using System.Collections.Generic;

namespace DllLoader.DataAccess
{
	public interface IRepository<T> where T : class
	{
		void Save(T data);
		IEnumerable<T> LoadAll();
	}
}
