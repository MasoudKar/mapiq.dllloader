﻿using System;
using System.Collections.Generic;

namespace DllLoader.DataAccess
{
	public class BookingRepository : IRepository<Booking>
	{

		List<Booking> Bookings;

		public BookingRepository()
		{
			Bookings = new List<Booking>() {
				{new Booking { Title="booking1", StartTime=new DateTime(2019, 6, 19, 13, 00, 00), EndTime = new DateTime(2019, 6, 19, 13, 30, 00)} },
				{new Booking { Title="booking2", StartTime=new DateTime(2019, 6, 19, 14, 00, 00), EndTime = new DateTime(2019, 6, 19, 14, 30, 00)} },
				{new Booking { Title="booking3", StartTime=new DateTime(2019, 6, 19, 15, 00, 00), EndTime = new DateTime(2019, 6, 19, 15, 30, 00)} }
			};
		}
		
		public void Save(Booking data)
		{
			Bookings.Add(data);
		}

		public IEnumerable<Booking> LoadAll()
		{
			return Bookings;
		}
	}
}
