﻿using System;
using System.Collections.Generic;

namespace DllLoader.DataAccess
{
	public class CustomerRepository : IRepository<Customer>
	{
		public IEnumerable<Customer> LoadAll()
		{
			return new List<Customer>
			{
				{new Customer{ Code = 1, AssemblyAddress = "PluginOne"} },
				{new Customer{ Code = 2, AssemblyAddress = "PluginTwo"} }
			};

		}

		public void Save(Customer data)
		{
			throw new NotImplementedException();
		}
	}

}
