﻿using System;

namespace DllLoader.DataAccess
{
	public class BookingDto
	{
		public int CustomerCode { get; set; }
		public string Title { get; set; }
		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }
	}
}
