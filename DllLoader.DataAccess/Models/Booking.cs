﻿using System;

namespace DllLoader.DataAccess
{
	public class Booking
	{
		public string Title { get; set; }
		public DateTime StartTime { get; set; }
		public DateTime EndTime { get; set; }
	}
}
